const UserService = require('./user.service')
const AppError = require('../utils/app.error')
const AppResult = require('../utils/app.result')
const emptyParam = require('../utils').emptyParam

const RESULT_LABEL = 'user'

exports.getUser = async (userId, queryFieldsParam) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }

  const user = await UserService.getUser({ _id: userId })
  if (!user) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'Identificador de usuario no válido' } })
  }

  const fieldsOptions = !emptyParam(queryFieldsParam) ? queryFieldsParam.split(',') : ['__short__']

  return new AppResult(null, 200, user.toJSON(fieldsOptions), RESULT_LABEL)
}

exports.partialUpdateUser = async (userId, bodyFields, avatarFile, protocol, host) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }

  const { password, newPassword, ...userData } = bodyFields

  const passwordRequired = emptyParam(password) && emptyParam(newPassword) ? '' : '+password'

  const user = await UserService.getUser({ _id: userId }, passwordRequired)
  if (!user) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'Identificador de usuario no válido' } })
  }

  if (avatarFile) {
    if (avatarFile.size > 512 * 1024) {
      throw new AppError('Validation', { 'avatar': { 'message': 'La imagen es demasiado grande' } })
    }
    const url = protocol + '://' + host
    userData.avatar = url + '/images/' + avatarFile.filename
  }
  if (userData.birthDate) {
    userData.birthDate = new Date(userData.birthDate)
    if (!(userData.birthDate instanceof Date && !isNaN(userData.birthDate))) {
      throw new AppError('Validation', { 'birthDate': { 'message': 'Formato o fecha inválida' } })
    }
  }
  Object.assign(user, userData)

  let msgResult = null
  if (user.password) {
    if (!await user.validatePassword(password)) {
      throw new AppError('Auth', { 'password': { 'message': 'Contraseña no válida' } })
    }
    await user.encryptPassword(newPassword)
    msgResult = 'Contraseña actualizada exitosamente'
  }
  await UserService.updateUser(user)

  return new AppResult(msgResult || 'Usuario actualizado exitosamente', 200, user.toJSON(['__short__']), RESULT_LABEL)
}
