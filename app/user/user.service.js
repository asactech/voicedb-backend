const User = require('../models/user')

exports.getUser = async (query, passwordRequired = '') => {
  return User.findOne(query).select(passwordRequired).exec()
}

exports.changePassword = async (user, password) => {
  await user.encryptPassword(password)
  return updateUser(user)
}

const updateUser = async (user) => {
  return user.save()
}

module.exports.updateUser = updateUser
