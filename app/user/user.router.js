const router = require('express').Router()
const modelRouter = require('../model/model.router')
const controllerHandler = require('../middlewares/handle.controller')
const UserController = require('./user.controller')
const trimMw = require('../utils').trimMw
const extractAvatar = require('../middlewares/extract-avatar')

router.route('/:userId')
  .get(
    controllerHandler(UserController.getUser, (req, res, next) => [req.params.userId, req.query.fields])
  )
  .patch(
    trimMw,
    extractAvatar,
    controllerHandler(UserController.partialUpdateUser, (req, res, next) => [
      req.params.userId, req.body, req.file, req.protocol, req.get('host')
    ])
  )
router.use('/:userId/models', modelRouter)

module.exports = router
