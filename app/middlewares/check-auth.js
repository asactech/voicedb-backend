const jwt = require('jsonwebtoken')
const vdb = require('../config').vdb
const AppError = require('../utils/app.error')
const errorHandler = require('./handle.error').errorHandler

module.exports = (req, res, next) => {
  try {
    const token = req.headers.authorization.split(' ')[1]
    const decodedToken = jwt.verify(token, vdb.auth_secret)
    req.credentials = { email: decodedToken.email, userId: decodedToken.userId }
    next()
  } catch (error) {
    return errorHandler(new AppError('Auth', null, 'Credenciales no válidas'), req, res, next)
  }
}
