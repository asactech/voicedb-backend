const { vdb } = require('../config')

exports.errorHandler = (error, req, res, next) => {
  if (!vdb.isProduction) {
    console.log(error)
  }
  error.httpCode = (error.httpCode) ? error.httpCode : (error.name === 'ValidationError') ? 422 : 500
  return res.status(error.httpCode).json({
    name: error.name,
    message: error.message,
    errors: (error.errors) && Object.keys(error.errors).reduce(function (errors, key) {
      errors[key] = error.errors[key].message
      return errors
    }, {})
  })
}
