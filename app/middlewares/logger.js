const { createLogger, format, transports } = require('winston')
const DailyRotateFile = require('winston-daily-rotate-file')
const fs = require('fs')
const path = require('path')
const { vdb } = require('../config')

const logDir = './app/logs'

if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir)
}

const filenameError = 'error-%DATE%.log'
const filenameCombined = 'combined-%DATE%.log'

const transportErrorFile = new (DailyRotateFile)({
  datePattern: 'YYYYMMDD',
  filename: filenameError,
  dirname: logDir,
  maxSize: '5m',
  maxFiles: '7d',
  level: 'error'
})
const transportCombinedFile = new (DailyRotateFile)({
  datePattern: 'YYYYMMDD',
  filename: filenameCombined,
  dirname: logDir,
  maxSize: '5m',
  maxFiles: '7d',
  level: 'info'
})
const transportsArr = [
  new transports.Console({
    level: 'info',
    format: format.combine(
      format.colorize({ all: true })
    )
  }),
  transportErrorFile
]
if (!vdb.isProduction) {
  transportsArr.push(transportCombinedFile)
}

const logger = caller => {
  return createLogger({
    format: format.combine(
      format.label({ label: path.basename(caller) }),
      format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      format.printf(
        info =>
          `[${info.timestamp} ${info.level.toUpperCase()} (${info.label})]: ${info.message}`
      )
    ),
    transports: [...transportsArr]
  })
}

module.exports = exports = logger
logger.transportErrorFile = transportErrorFile
logger.transportCombinedFile = transportCombinedFile
