exports.resultHandler = (result, req, res, next) => {
  console.log(result)
  const httpCode = (result.httpCode) ? result.httpCode : 200
  delete result.httpCode
  return res.status(httpCode).json(result)
}
