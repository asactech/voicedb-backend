const expressWinston = require('express-winston')
const { transportErrorFile, transportCombinedFile } = require('./logger')
const { vdb } = require('../config')

expressWinston.requestWhitelist.push('body')
expressWinston.responseWhitelist.push('body')

const transportsArr = [
  transportErrorFile
]
if (!vdb.isProduction) {
  transportsArr.push(transportCombinedFile)
}

const expressLogger = expressWinston.logger({
  transports: [...transportsArr],
  meta: true,
  msg: (req, res) => {
    let obj = {
      remoteAddr: req.connection.remoteAddress,
      method: req.method,
      statusCode: res.statusCode,
      url: req.url,
      query: req.query
    }
    if (!vdb.isProduction) {
      console.table(obj)
      console.log(`Request body ${Object.keys(req.body || {}).length ? JSON.stringify(req.body || {}, null, 2) : ''}`)
      console.log(`Response body ${Object.keys(res.body || {}).length ? JSON.stringify(res.body || {}, null, 2) : ''}`)
    }
    return '{{req.protocol.toUpperCase()}}{{req.httpVersion}} - {{req.connection.remoteAddress}}'
  },
  expressFormat: false,
  colorStatus: true,
  ignoreRoute: function (req, res) { return false }
})

module.exports = expressLogger
