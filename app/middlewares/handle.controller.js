const { errorHandler } = require('./handle.error')
const { resultHandler } = require('./handle.result')

const controllerHandler = (promise, params) => async (req, res, next) => {
  const boundParams = params ? params(req, res, next) : []
  try {
    const result = await promise(...boundParams)
    return resultHandler(result, req, res, next)
  } catch (error) {
    return errorHandler(error, req, res, next)
  }
}

module.exports = controllerHandler
