const app = require('./index')
const logger = require('./middlewares/logger')(__filename)
const config = require('./config')

app.listen(config.express.port, config.express.ip, function (error) {
  if (error) {
    logger.error('Unable to listen for connections', error)
    process.exit(10)
  }
  logger.info(`express is listening on http://${config.express.ip}:${config.express.port}`)
})
