const router = require('express').Router({ mergeParams: true })
const controllerHandler = require('../middlewares/handle.controller')
const ModelController = require('./model.controller')
const trimMw = require('../utils').trimMw

router.route('')
  .get(
    controllerHandler(ModelController.getModels, (req, res, next) => [
      req.credentials.userId, req.params.userId, req.query.fields
    ])
  )
  .post(
    trimMw,
    controllerHandler(ModelController.createModel, (req, res, next) => [
      req.credentials.userId, req.params.userId, req.body.name
    ])
  )
router.route('/:modelId')
  .get(
    controllerHandler(ModelController.getModel, (req, res, next) => [
      req.credentials.userId, req.params.userId, req.params.modelId, req.query.fields
    ])
  )
  .patch(
    trimMw,
    controllerHandler(ModelController.partialUpdateModel, (req, res, next) => [
      req.credentials.userId, req.params.userId, req.params.modelId, req.body
    ])
  )
  .delete(
    controllerHandler(ModelController.deleteModel, (req, res, next) => [
      req.credentials.userId, req.params.userId, req.params.modelId, req.query.fields
    ])
  )

module.exports = router
