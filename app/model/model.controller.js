const ModelService = require('./model.service')
const AppResult = require('../utils/app.result')
const AppError = require('../utils/app.error')
const emptyParam = require('../utils').emptyParam
const ObjectId = require('mongoose').Types.ObjectId

const RESULT_LABEL = 'model'
const RESULT_PLURAL_LABEL = RESULT_LABEL + 's'

exports.getModels = async (authUserId, userId, queryFieldsParam) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }
  if (!ObjectId.isValid(userId) || authUserId != userId) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es inválido' } })
  }

  const models = await ModelService.getModels({ userId: userId })

  const fieldsOptions = !emptyParam(queryFieldsParam) ? queryFieldsParam.split(',') : ['__short__']

  return new AppResult(null, 200, models.map(model => model.toJSON(fieldsOptions)), RESULT_PLURAL_LABEL)
}

exports.getModel = async (authUserId, userId, modelId, queryFieldsParam) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }
  if (emptyParam(modelId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del modelo es requerido' } })
  }
  if (!ObjectId.isValid(userId) || authUserId != userId) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es inválido' } })
  }
  if (!ObjectId.isValid(modelId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del modelo es inválido' } })
  }

  const model = await ModelService.getModel({ _id: modelId, userId: userId })

  if (!model) {
    throw new AppError('BadRequest')
  }

  const fieldsOptions = !emptyParam(queryFieldsParam) ? queryFieldsParam.split(',') : ['__all__']

  return new AppResult(null, 200, model.toJSON(fieldsOptions), RESULT_PLURAL_LABEL)
}

exports.deleteModel = async (authUserId, userId, modelId) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }
  if (emptyParam(modelId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del modelo es requerido' } })
  }
  if (!ObjectId.isValid(userId) || authUserId != userId) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es inválido' } })
  }
  if (!ObjectId.isValid(modelId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del modelo es inválido' } })
  }

  const model = await ModelService.getModel({ _id: modelId, userId: userId })

  if (!model) {
    throw new AppError('BadRequest')
  }

  await ModelService.deleteModel(model)

  return new AppResult('Modelo eliminado exitosamente', 200)
}

exports.createModel = async (authUserId, userId, name) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }
  if (emptyParam(name)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El nombre del modelo es requerido' } })
  }
  if (!ObjectId.isValid(userId) || authUserId != userId) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es inválido' } })
  }

  const models = await ModelService.getModels({ userId: userId, name: name })

  if (models.length > 0) {
    throw new AppError('Validation', { 'name': { 'message': 'El nombre del modelo ya se encuentra registrado' } })
  }

  const model = await ModelService.createModel({ userId: userId, name: name })

  return new AppResult('Modelo creado exitosamente', 200, model.toJSON(['__short__']))
}

exports.partialUpdateModel = async (authUserId, paramUserId, modelId, bodyFields) => {
  if (emptyParam(paramUserId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }
  if (emptyParam(modelId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del modelo es requerido' } })
  }
  if (!ObjectId.isValid(paramUserId) || authUserId != paramUserId) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es inválido' } })
  }
  if (!ObjectId.isValid(modelId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del modelo es inválido' } })
  }

  const model = await ModelService.getModel({ _id: modelId, userId: paramUserId })

  if (!model) {
    throw new AppError('BadRequest')
  }

  const { userId, ...modelData } = bodyFields
  modelData.updatedAt = new Date()
  if (modelData.nodeDataArray || modelData.linkDataArray) {
    modelData.updatedModelAt = modelData.updatedAt
  }
  Object.assign(model, modelData)

  await ModelService.updateModel(model)

  return new AppResult('Modelo actualizado exitosamente', 200)
}