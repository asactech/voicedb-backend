const Model = require('../models/model')

exports.createModel = async (modelData) => {
  return Model.create(modelData)
}

exports.getModels = async (query) => {
  return Model.find(query).exec()
}

exports.getModel = async (query) => {
  return Model.findOne(query).exec()
}

exports.updateModel = async (model) => {
  return model.save()
}

exports.deleteModel = async (model) => {
  return model.remove()
}