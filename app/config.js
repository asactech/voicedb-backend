var config = module.exports

config.vdb = {
  isProduction: process.env.VDB_NODE_ENV === 'production',
  auth_secret: process.env.VDB_AUTH_SECRET,
  reset_secret: process.env.VDB_RESET_SECRET,
  client: {
    url: process.env.VDB_CLIENT_URL
  }
}

config.express = {
  ip: process.env.VDB_EXPRESS_IP,
  port: process.env.VDB_EXPRESS_PORT
}

config.mongodb = {
  prefix: process.env.VDB_MONGODB_PREFIX,
  user: process.env.VDB_MONGODB_USER,
  host: process.env.VDB_MONGODB_HOST,
  db: process.env.VDB_MONGODB_DB
}

config.mail = {
  provider: {
    gmail: {
      host: process.env.VDB_MAIL_PROVIDER_GMAIL_HOST,
      port: process.env.VDB_MAIL_PROVIDER_GMAIL_PORT,
      credentials: {
        type: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_TYPE,
        user: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_USER,
        clientId: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_CLIENTID,
        clientSecret: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_CLIENTSECRET,
        accessToken: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_ACCESSTOKEN,
        refreshToken: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_REFRESHTOKEN,
        expires: process.env.VDB_MAIL_PROVIDER_GMAIL_AUTH_EXPIRESTOKEN
      }
    },
    local: {
      host: process.env.VDB_MAIL_PROVIDER_LOCAL_HOST,
      port: process.env.VDB_MAIL_PROVIDER_LOCAL_PORT
    },
    production: {
      host: process.env.VDB_MAIL_PROVIDER_PRODUCTION_HOST,
      port: process.env.VDB_MAIL_PROVIDER_PRODUCTION_PORT,
      credentials: {
        user: process.env.VDB_MAIL_PROVIDER_PRODUCTION_AUTH_USER,
        pass: process.env.VDB_MAIL_PROVIDER_PRODUCTION_AUTH_PASS
      }
    },
  }
}
