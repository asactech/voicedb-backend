const AuthService = require('./auth.service')
const UserService = require('../user/user.service')
const AppError = require('../utils/app.error')
const AppResult = require('../utils/app.result')
const emptyParam = require('../utils').emptyParam

const RESULT_LABEL = 'user'

exports.signup = async (email, password) => {
  if (emptyParam(email)) {
    throw new AppError('BadRequest', { 'email': { 'message': 'El correo electrónico es requerido' } })
  }

  if (emptyParam(password)) {
    throw new AppError('BadRequest', { 'password': { 'message': 'La constraseña es requerida' } })
  }

  await AuthService.signup({ email: email, password: password })

  return new AppResult('Usuario creado exitosamente', 201)
}

exports.login = async (email, password) => {
  const user = await UserService.getUser({ email: email }, '+password')
  if (!user) {
    throw new AppError('Auth', { 'email': { 'message': 'Correo electrónico no válido' } })
  }

  if (!await user.validatePassword(password)) {
    throw new AppError('Auth', { 'password': { 'message': 'Contraseña no válida' } })
  }

  user.loggedAt = new Date()
  await UserService.updateUser(user)

  return new AppResult(null, 200, user.toAuthJSON(), RESULT_LABEL)
}

exports.recovery = async (email) => {
  const user = await UserService.getUser({ email: email }, '+password')
  if (!user) {
    throw new AppError('Auth', { 'email': { 'message': 'Correo electrónico no válido' } })
  }

  await AuthService.sendPasswordResetMail(user)

  return new AppResult('Se ha enviado el enlace de recuperación a su correo', 200)
}

exports.reset = async (userId, token, password, confirmPassword) => {
  if (emptyParam(userId)) {
    throw new AppError('BadRequest', { 'userId': { 'message': 'El identificador del usuario es requerido' } })
  }
  if (emptyParam(token)) {
    throw new AppError('BadRequest', { 'token': { 'message': 'El token es requerido' } })
  }
  if (emptyParam(password)) {
    throw new AppError('BadRequest', { 'password': { 'message': 'La contraseña es requerida' } })
  }
  if (emptyParam(confirmPassword)) {
    throw new AppError('BadRequest', { 'confirmPassword': { 'message': 'La confirmación de contraseña es requerida' } })
  }

  const passwordResetData = await AuthService.decodePasswordReset(userId, token)
  Object.entries(passwordResetData).map((entrie) => {
    if (!entrie[1]) {
      throw new AppError('BadRequest')
    }
  })

  if (passwordResetData.expiresAt < +new Date()) {
    throw new AppError('AuthError', { 'token': { 'message': 'El token ha caducado o ya ha sido usado' } })
  }

  const user = await UserService.getUser({ _id: passwordResetData.userId }, '+password')
  if (!user) {
    throw new AppError('Auth')
  }

  if (!await user.validateResetToken(passwordResetData.expiresAt, passwordResetData.userEncrypted)) {
    throw new AppError('Auth')
  }

  if (password !== confirmPassword) {
    throw new AppError('Auth', { 'confirmPassword': { 'message': 'Las contraseñas no coinciden' } })
  }

  await UserService.changePassword(user, confirmPassword)

  return new AppResult('Reinicio de contraseña procesado exitosamente', 200)
}
