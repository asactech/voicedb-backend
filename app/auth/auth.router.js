const router = require('express').Router()
const controllerHandler = require('../middlewares/handle.controller')
const AuthController = require('./auth.controller')
const trimMw = require('../utils').trimMw

router.post('/signup',
  trimMw,
  controllerHandler(AuthController.signup, (req, res, next) => [req.body.email, req.body.password])
)
router.post('/login',
  controllerHandler(AuthController.login, (req, res, next) => [req.body.email, req.body.password])
)
router.post('/recovery',
  controllerHandler(AuthController.recovery, (req, res, next) => [req.body.email])
)
router.patch('/reset',
  trimMw,
  controllerHandler(AuthController.reset, (req, res, next) => [
    req.body.id, req.body.token, req.body.password, req.body.confirmPassword
  ])
)

module.exports = router
