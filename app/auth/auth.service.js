const User = require('../models/user')

const vdb = require('../config').vdb
const templates = require('../templates/email/reset.email')
const transport = require('../utils/local.transport')
const convert = require('../utils/crypt').convert

exports.signup = async (userData) => {
  const user = new User(userData)
  await user.encryptPassword(user.password)
  return User.create(user)
}

exports.sendPasswordResetMail = async (user) => {
  const tokenData = await user.generateResetToken()
  const tokenUrl = `${vdb.client.url}/auth/reset/${tokenData.token}`
  const resetPasswordUrl = `${vdb.client.url}/auth/recovery`

  const htmlTemplate = templates.resetHtmlEmailTemplate(tokenUrl)
  const textTemplate = templates.resetTextEmailTemplate(tokenUrl, resetPasswordUrl, tokenData.expiresIn)
  const mailOptions = {
    from: 'VoiceDB <vdb@globalis4u.com.ve>',
    to: user.email,
    subject: '[VoiceDB] Por favor reinicia tu contraseña',
    text: textTemplate,
    html: htmlTemplate
  }

  return transport.sendMail(mailOptions)
}

exports.decodePasswordReset = async (userId, token) => {
  userId = convert(userId, 'hex', 'utf8')
  var [ expiresAt, userEncrypted ] = token.split('-')
  expiresAt = convert(expiresAt, 'hex', 'utf8')
  expiresAt = isNaN(expiresAt) ? null : Number(expiresAt)
  return { userId, expiresAt, userEncrypted }
}
