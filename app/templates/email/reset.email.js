/* exports.resetHtmlEmailTemplate = `
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Recuperar contraseña</title>
    <style type="text/css">

      #outlook a {
        padding:0;
      }
      body {
        font-family: 'Poppins', sans-serif;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0; 
        padding: 0;
      }
      .ExternalClass {
        width:100%;
      }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
        line-height: 100%;
      }
      img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
      }
      a img {
        border: none;
        display: inline-block;
      }
      .image_fix {
        display: block;
      }
      table td {
        border-collapse: collapse;
      }
      table {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      a {
        text-decoration: none;
      }
      p {
        padding: 0 10px 0 10px;;
        color: #FFFFFF;
        font-size: 16px;
        line-height: 160%;
        font-weight: 300;
      }
      a.link2{
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 3.5px;
        color: #007c57;
        border-radius: 4px;
        text-decoration: none;
      }
      h2 {
        color: #DAE6E6;
        font-size: 24px;
        font-weight: 500;
      }
      .bgItem{
        background: #00AC84;
      }
      .bgBody{
        background: #243033;
      }
    </style>
    <script type="colorScheme" class="swatch active">
      {
        "name":"Default",
        "bgBody":"ffffff",
        "link":"f2f2f2",
        "color":"555555",
        "bgItem":"F4A81C",
        "title":"181818"
      }
    </script>
  </head>
  <body>
    <table cellpadding="0" width="100%" cellspacing="0" border="0" class='bgBody'>
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;">
            <tr>
              <td class='movableContentContainer'>
                
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                    <tr height="40">
                      <td width="100%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="100%" valign="top" align="center">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" >
                            <img src="https://dummyimage.com/155x155/DAE6E6/00ac84&text=VoiceDB+Logo" width="155" height='155' alt='Logo' data-default="placeholder"/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr height="25">
                      <td width="100%">&nbsp;</td>
                    </tr>
                  </table>
                </div>
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                    <tr>
                      <td width="100%" colspan="3" align="center" style="padding-bottom:10px;padding-top:25px;">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" >
                            <h2 >Recupera tu contraseña</h2>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" align="center" style="padding-bottom:5px;">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" >
                            <p >Para reiniciar tu contraseña solo debes hacer click en el botón, que se encuentra acá abajo, y seguir las instrucciones.</p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                    <tr>
                      <td width="100%" align="center" style="padding-top:25px; padding-bottom:115px; ">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="200" height="50">
                          <tr>
                            <td bgcolor="#DAE6E6" align="center" style="border-radius:10px;" width="200" height="50">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" >
                                  <a target='_blank' href="{{ tokenUrl }}" class='link2'>REINICIAR CONTRASEÑA</a>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;">
                    <tr>
                      <td class='bgItem' style="color: #FFFFFF">
                        <table cellpadding="0" style="border-collapse:collapse;" cellspacing="0" border="0" align="center" width="600">
                          <tr>
                            <td width="50%" style="vertical-align:bottom;">
                              <div class="contentEditableContainer contentImageEditable">
                                <div class="contentEditable" >
                                  <div style="padding:20px 0 20px 0; text-align:center;">
                                    <img src="https://dummyimage.com/150x150/ffffff/00ac84&text=Talking+Cloud" width="150" data-default="placeholder" data-max-width="150"/>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td width="50%" valign="top" style="padding: 5% 0 5% 2%; color: #FFFFFF;">
                              <br/>
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" >
                                  <span style="font-size:16px; line-height:200%;"><strong>VoiceDB</strong></span>
                                  <br/><br/>
                                  <span style="font-size:14px; line-height:200%;">[COMPANY.ADDRESS]</span>
                                  <br/>
                                  <span style="font-size:14px; line-height:200%;">[COMPANY.PHONE]</span>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
`
exports.resetTextEmailTemplate = `
Nosotros escuchamos que perdiste la contraseña de VoiceDB, lo cual lamentamos.

Pero no te preocupes! Puedes usar el siguiente enlace para reiniciar tu contraseña:

{{ tokenUrl }}


Si no usas este enlace en {{ expiresIn }} horas, caducará. Para obtener un nuevo enlace de restablecimiento de contraseña, visite: {{ resetPasswordUrl }}

Gracias,
Tus amigos en VoiceDB
`
 */

exports.resetHtmlEmailTemplate = (tokenUrl) => {
  return `
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Recuperar contraseña</title>
    <style type="text/css">

      #outlook a {
        padding:0;
      }
      body {
        font-family: 'Poppins', sans-serif;
        -webkit-text-size-adjust: 100%;
        -ms-text-size-adjust: 100%;
        margin: 0; 
        padding: 0;
      }
      .ExternalClass {
        width:100%;
      }
      .ExternalClass,
      .ExternalClass p,
      .ExternalClass span,
      .ExternalClass font,
      .ExternalClass td,
      .ExternalClass div {
        line-height: 100%;
      }
      img {
        outline: none;
        text-decoration: none;
        -ms-interpolation-mode: bicubic;
      }
      a img {
        border: none;
        display: inline-block;
      }
      .image_fix {
        display: block;
      }
      table td {
        border-collapse: collapse;
      }
      table {
        border-collapse: collapse;
        mso-table-lspace: 0pt;
        mso-table-rspace: 0pt;
      }
      a {
        text-decoration: none;
      }
      p {
        padding: 0 10px 0 10px;;
        color: #FFFFFF;
        font-size: 16px;
        line-height: 160%;
        font-weight: 300;
      }
      a.link2{
        font-size: 14px;
        font-weight: 500;
        letter-spacing: 3.5px;
        color: #007c57;
        border-radius: 4px;
        text-decoration: none;
      }
      h2 {
        color: #DAE6E6;
        font-size: 24px;
        font-weight: 500;
      }
      .bgItem{
        background: #00AC84;
      }
      .bgBody{
        background: #243033;
      }
    </style>
    <script type="colorScheme" class="swatch active">
      {
        "name":"Default",
        "bgBody":"ffffff",
        "link":"f2f2f2",
        "color":"555555",
        "bgItem":"F4A81C",
        "title":"181818"
      }
    </script>
  </head>
  <body>
    <table cellpadding="0" width="100%" cellspacing="0" border="0" class='bgBody'>
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;">
            <tr>
              <td class='movableContentContainer'>
                
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                    <tr height="40">
                      <td width="100%">&nbsp;</td>
                    </tr>
                    <tr>
                      <td width="100%" valign="top" align="center">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" >
                            <img src="https://dummyimage.com/155x155/DAE6E6/00ac84&text=VoiceDB+Logo" width="155" height='155' alt='Logo' data-default="placeholder"/>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr height="25">
                      <td width="100%">&nbsp;</td>
                    </tr>
                  </table>
                </div>
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                    <tr>
                      <td width="100%" colspan="3" align="center" style="padding-bottom:10px;padding-top:25px;">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" >
                            <h2 >Recupera tu contraseña</h2>
                          </div>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td width="100%" align="center" style="padding-bottom:5px;">
                        <div class="contentEditableContainer contentTextEditable">
                          <div class="contentEditable" >
                            <p >Para reiniciar tu contraseña solo debes hacer click en el botón, que se encuentra acá abajo, y seguir las instrucciones.</p>
                          </div>
                        </div>
                      </td>
                    </tr>
                  </table>
                </div>
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="600">
                    <tr>
                      <td width="100%" align="center" style="padding-top:25px; padding-bottom:115px; ">
                        <table cellpadding="0" cellspacing="0" border="0" align="center" width="200" height="50">
                          <tr>
                            <td bgcolor="#DAE6E6" align="center" style="border-radius:10px;" width="200" height="50">
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" >
                                  <a target='_blank' href="${tokenUrl}" class='link2'>REINICIAR CONTRASEÑA</a>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
                <div class='movableContent'>
                  <table cellpadding="0" cellspacing="0" border="0" align="center" width="100%" style="border-collapse:collapse;">
                    <tr>
                      <td class='bgItem' style="color: #FFFFFF">
                        <table cellpadding="0" style="border-collapse:collapse;" cellspacing="0" border="0" align="center" width="600">
                          <tr>
                            <td width="50%" style="vertical-align:bottom;">
                              <div class="contentEditableContainer contentImageEditable">
                                <div class="contentEditable" >
                                  <div style="padding:20px 0 20px 0; text-align:center;">
                                    <img src="https://dummyimage.com/150x150/ffffff/00ac84&text=Talking+Cloud" width="150" data-default="placeholder" data-max-width="150"/>
                                  </div>
                                </div>
                              </div>
                            </td>
                            <td width="50%" valign="top" style="padding: 5% 0 5% 2%; color: #FFFFFF;">
                              <br/>
                              <div class="contentEditableContainer contentTextEditable">
                                <div class="contentEditable" >
                                  <span style="font-size:16px; line-height:200%;"><strong>VoiceDB</strong></span>
                                  <br/><br/>
                                  <span style="font-size:14px; line-height:200%;">[COMPANY.ADDRESS]</span>
                                  <br/>
                                  <span style="font-size:14px; line-height:200%;">[COMPANY.PHONE]</span>
                                </div>
                              </div>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </div>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
  </body>
</html>
  `
}

exports.resetTextEmailTemplate = (tokenUrl, resetPasswordUrl, expiresIn) => {
  return `
Nosotros escuchamos que perdiste la contraseña de VoiceDB, lo cual lamentamos.

Pero no te preocupes! Puedes usar el siguiente enlace para reiniciar tu contraseña:

${tokenUrl}


Si no usas este enlace en ${expiresIn} horas, caducará. Para obtener un nuevo enlace de restablecimiento de contraseña, visite: ${resetPasswordUrl}

Gracias,
Tus amigos en VoiceDB
`
}
