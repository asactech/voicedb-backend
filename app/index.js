require('dotenv').config()
const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const { vdb, mongodb } = require('./config')
const mongoose = require('mongoose')
const logger = require('./middlewares/logger')(__filename)
const expressLogger = require('./middlewares/express-logger')
const cors = require('./middlewares/cors').cors
const cache = require('./middlewares/cache').cache

const app = express()

mongoose.connect(
  `${mongodb.prefix}://${mongodb.user}${mongodb.host}/${mongodb.db}`,
  { useCreateIndex: true, useNewUrlParser: true }
)
  .then(() => {
    mongoose.set('debug', !vdb.isProduction)
    logger.info('Connected to database!')
  })
  .catch(() => {
    logger.warn('Connection to database failed!')
  })

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())
app.use(expressLogger)
app.use('/images', express.static(path.join('app/public/images')))
app.use(cors)
app.use(cache)

app.use(require('./routes'))

app.use(function (req, res, next) {
  var err = new Error('Not Found')
  err.httpCode = 404
  next(err)
})

if (!vdb.isProduction) {
  app.use(function (err, req, res, next) {
    logger.error(err.stack)
    res.status(err.httpCode || 500).json({
      message: err.message,
      error: err
    })
  })
}

app.use(function (err, req, res, next) {
  res.status(err.httpCode || 500).json({
    message: err.message,
    error: {}
  })
})

module.exports = app
