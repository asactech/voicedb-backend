class AppError extends Error {
  constructor (name, errors = null, message = null, httpCode = null) {
    super(message)
    this.name = name + 'Error'
    switch (name) {
      case 'Auth':
        this.message = message || 'Credenciales de autenticación no válidas'
        this.httpCode = httpCode || 401
        break
      case 'NotFound':
        this.message = message || 'El recurso solicitado no existe'
        this.httpCode = httpCode || 404
        break
      case 'BadRequest':
        this.message = message || 'La información enviada no puede ser procesada'
        this.httpCode = httpCode || 400
        break
      case 'Validation':
        this.message = message || 'La información no cumple con las validaciones del sistema'
        this.httpCode = httpCode || 422
        break
      default:
        this.message = message || 'Error inesperado en el servidor'
        this.httpCode = httpCode || 500
    }
    Object.assign(this, errors && { errors: errors })
  }
}

module.exports = AppError
