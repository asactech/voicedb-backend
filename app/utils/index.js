exports.emptyParam = (field) => {
  return (!field || !field.trim())
}

// TODO: Pasar a middlewares
exports.trimMw = (req, res, next) => {
  req.body = Object.entries(req.body)
    .map(([key, val]) => [key, (typeof val === 'string') ? val.trim() : val])
    .reduce((obj, [k, v]) => ({ ...obj, [k]: v }), {})
  next()
}
