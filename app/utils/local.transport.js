const nodemailer = require('nodemailer')
const localConfig = require('../config').mail.provider.local

const localTransport = nodemailer.createTransport({
  host: localConfig.host,
  port: localConfig.port
})

module.exports = localTransport
