const nodemailer = require('nodemailer')
const mailConfig = require('../config').mail

const gmailTransport = nodemailer.createTransport({
  host: mailConfig.provider.production.host,
  port: mailConfig.provider.production.port,
  secure: true,
  auth: mailConfig.provider.production.credentials
})

/* gmailTransport.on('token', token => {
  console.log('A new access token was generated')
  console.log('User: %s', token.user)
  console.log('Access Token: %s', token.accessToken)
  console.log('Expires: %s', new Date(token.expires))
}) */

module.exports = gmailTransport
