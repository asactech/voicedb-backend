const { createHmac, timingSafeEqual } = require('crypto')

exports.encrypt = (dataToEncrypt, secret) => {
  const hmac = createHmac('sha256', secret)
  hmac.update(dataToEncrypt)
  return hmac.digest('hex')
}

exports.compare = (a, b) => {
  return timingSafeEqual(Buffer.from(a), Buffer.from(b))
}

exports.convert = (data, from, to) => {
  return Buffer.from(data, from).toString(to)
}
