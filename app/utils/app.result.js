class AppResult {
  constructor (message = null, httpCode = 200, content = null, label = null) {
    Object.assign(this, message && { message: message })
    this.httpCode = httpCode
    Object.assign(this, (label) ? { [label]: content } : content)
  }
}

module.exports = AppResult
