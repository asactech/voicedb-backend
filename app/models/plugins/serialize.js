module.exports = exports = function serializePlugin (schema, options) {
  schema.statics.toJSON = function (tempObj, fieldsOptions) {
    const serialize = (tempObj, fields) => {
      return Object.keys(tempObj._doc)
        .filter(key => fields.includes(key))
        .reduce((obj, key) => {
          return {
            ...obj,
            [key]: tempObj[key]
          }
        }, {})
    }
    let fields = ''
    switch (fieldsOptions[0]) {
      case '__all__':
        return tempObj._doc
      case '__short__':
        fields = options.fields['__short__']
        break
      default:
        fields = fieldsOptions
    }
    return serialize(tempObj, fields)
  }

  schema.methods.toJSON = function (fieldsOptions) {
    return schema.statics.toJSON(this, fieldsOptions)
  }
}
