const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')
const serializePlugin = require('./plugins/serialize')

const { compare, hash } = require('bcrypt')
const { encrypt, compare: verify, convert } = require('../utils/crypt')
const jwt = require('jsonwebtoken')
const vdb = require('../config').vdb

const UserSchema = mongoose.Schema({
  email: {
    type: String,
    required: [true, 'El correo electrónico es requerido'],
    trim: true,
    unique: true,
    index: true
  },
  password: {
    type: String,
    required: [true, 'La contraseña es requerida'],
    trim: true,
    select: false
  },
  avatar: { type: String, trim: true },
  fullName: { type: String, trim: true },
  birthDate: { type: Date },
  occupation: { type: String, trim: true },
  companyOrEducation: { type: String, trim: true },
  country: { type: String, trim: true },
  loggedAt: { type: Date }
},
{
  timestamps: true
})

UserSchema.plugin(uniqueValidator, { message: 'El correo electrónico {VALUE} ya se encuentra registrado' })
UserSchema.plugin(serializePlugin, { fields: { __short__: ['email', 'avatar', 'fullName'] } })

UserSchema.methods.encryptPassword = async function (password) {
  this.password = await hash(password, 10)
}

UserSchema.methods.validatePassword = function (password) {
  return compare(password, this.password)
}

UserSchema.methods.generateJWT = function () {
  var exp = new Date(+new Date() + 12 * 60 * 60 * 1000)
  const token = jwt.sign({ userId: this._id, email: this.email }, vdb.auth_secret, { expiresIn: '12h', notBefore: '100' })

  return {
    token: token,
    expiresAt: exp.toISOString()
  }
}

UserSchema.methods.generateResetToken = async function () {
  const hours = 3
  const expiresAt = +new Date() + hours * 60 * 60 * 1000
  const userIdEncrypted = convert(this._id.toString(), 'utf8', 'hex')
  const userEncrypted = encrypt(this._id + this.password + this.email + this.loggedIn + expiresAt, vdb.reset_secret)
  return {
    token: `${userIdEncrypted}/${convert(expiresAt.toString(), 'utf8', 'hex')}-${userEncrypted}`,
    expiresIn: hours
  }
}

UserSchema.methods.validateResetToken = async function (expiresAt, hash) {
  const userEncrypted = encrypt(this._id + this.password + this.email + this.loggedIn + expiresAt, vdb.reset_secret)
  return userEncrypted.length === hash.length && verify(hash, userEncrypted)
}

UserSchema.methods.toAuthJSON = function () {
  const tokenInfo = this.generateJWT()
  return {
    id: this._id,
    token: tokenInfo.token,
    expiresAt: tokenInfo.expiresAt
  }
}

module.exports = mongoose.model('User', UserSchema)
