const mongoose = require('mongoose')

// const serialize = require('../utils').serialize
const serializePlugin = require('./plugins/serialize')

const ModelSchema = mongoose.Schema({
  userId: {
    type: 'ObjectId',
    ref: 'User',
    required: [true, 'El identificador del usuario es requerido']
  },
  name: {
    type: String,
    required: [true, 'El correo electrónico es requerido'],
    trim: true
  },
  preview: { type: String, trim: true },
  updatedModelAt: { type: Date },
  nodeDataArray: [{
    _id: false,
    key: {
      type: String,
      required: [true, 'El nombre de la entidad es requerido'],
      trim: true
    },
    items: [{
      _id: false,
      name: {
        type: String,
        required: [true, 'El nombre del atributo es requerido'],
        trim: true
      },
      iskey: {
        type: Boolean,
        required: [true, 'Debe indicar si es llave primaria o no'],
      }
    }],
    loc: {
      type: String,
      required: [true, 'La localización de la entidad es requerida'],
      trim: true
    }
  }],
  linkDataArray: [{
    _id: false,
    from: {
      type: String,
      required: [true, 'El nombre de la entidad origen es requerida'],
      trim: true
    },
    to: {
      type: String,
      required: [true, 'El nombre de la entidad destino es requerida'],
      trim: true
    },
    fromArrow: {
      type: String,
      required: [true, 'La cardinalidad en la entidad origen es requerida'],
      trim: true
    },
    toArrow: {
      type: String,
      required: [true, 'La cardinalidad en la entidad destino es requerida'],
      trim: true
    }
  }]
},
{
  timestamps: true
})

ModelSchema.plugin(serializePlugin, { fields: { __short__: ['_id', 'name', 'preview', 'updatedModelAt', 'createdAt'] } })

module.exports = mongoose.model('Model', ModelSchema)
