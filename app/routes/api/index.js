const router = require('express').Router()
const checkAuth = require('../../middlewares/check-auth')

router.use('/auth', require('../../auth/auth.router'))
router.use('/users', checkAuth, require('../../user/user.router'))
router.use('/models', checkAuth, require('../../model/model.router'))

module.exports = router
